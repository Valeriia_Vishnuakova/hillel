# Задание 1

my_list = [20, 45, 120, 48]
for number in my_list:
    if number > 100:
        print(number)

# Задание 2

my_list = [20, 45, 120, 48]
my_results = []
for number in my_list:
    if number > 100:
        my_results.append(number)

print(my_results)

# Задание 3

my_list = [20]
if len(my_list) < 2:
    my_list.append(0)


if len(my_list) >= 2:
#???? не поняла условие "то добавляется количество последних двух элементов"

    print(my_list)

# Задание 4

my_string = "0123456789"
my_string_2 = list(range(0, 100))
my_string_2 = str(my_string_2)

for symbol_1 in my_string:
    for symbol_2 in my_string_2:
        print(symbol_1 + symbol_2)